.TL
OMEMO over Discord

.AU
Luna Mendes

.AB
This paper describes what implementors could do to provide end-to-end encryption
on Discord using the OMEMO standard from XMPP.

This is a draft document.
.AE

.NH
Disclaimer

.PP
An OMEMO session can not be fully carried out over Discord due to limitations
of the Discord API. This paper goes in more detail on overcoming such
limitations using a third-party.

.NH
Key negotiation / distribution

.PP
Negotiating keys and prekeys in OMEMO use the User's originating XMPP server for
key storage and XEP-0163: Personal Eventing Protocol to signal device
key fetches and changes.

.PP
Discord does not provide any semantics to what XEP-0163 provides, so
implementors are allowed to improvise, and implement a generic pub/sub server.
.B "However"
notice that the protocol such a server provides can leak metadata about who is
talking with who, and so, extra care must be given to implementations going down
such paths.

.PP
.nr step 1 1
Another approach would be leveraging existing Discord mechanics to provide key
fingerprint material to the users via client profiles, however such an approach
would not work because:

.IP \n[step] 2
The maximum size for profile entries is too low to fit key material.

.IP \n+[step]
Discord does not send USER_UPDATE events when those change. The client would
need to send a DM to every user about the key change, so that the other users
fetch the new user profile with the new keys.

.NH
Key verification

.PP
Keys must be verified out of band to prevent a MITM attack by Discord when
negotiating the keys.

.NH
On the subject of search

.PP
Message search is something that is claimed to not work once you decide on using
a cryptosystem, however, search is possible. There are two approaches to it:

.NH 2
Client-side search

.PP
The first one decides that the Discord Client stores the message plaintext on
itself. The first obvious drawback is the overhead a client must have to do a
search, and that is even more expensive when you consider the Web technologies
powering the Client.

.NH 2
Server-side search

.PP
This involves a
.I "separate"
server that the client can send encrypted message history to. 

.PP
When searching, the client can ask for a batch of messages, decrypt, then
search through them. However the obvious drawbacks are that you need a separate
server with storage, and the implied bandwidth and decryption costs of searching
this way.

.NH 2
Conclusion

.PP
All XMPP clients use client-side message storage for unencrypted operation.
However, due to how Discord works, users assume the storage is server-side,
causing a conflict of interests in the part of what users assume Discord works
onto how a theoretical OMEMO-over-Discord provider provides.

.NH
On the subject of JavaScript

.PP
Notice that all the work described here can go to waste if Discord is still able
to send you maliciously crafted JavaScript for the client to execute and dump
all its private keys to Discord.

.PP
Because of that, it is recommended to design a new client that is fully native
and extensible to carry on the high expectations of a cryptosystem. "Signing
a plugin" is not enough, as Discord can still send their own JavaScript that
messes with the plugin's operation after signature verification.

.PP
The bottom line is that if you can't trust the runtime then it is not secure.
Another case where this line of thought is valid is in ProtonMail, where they
can send you malicious JavaScript that just records everything. ProtonMail
answers that such attacks are not worrisome when we have things like SNI.

.PP
The author is in the "side" that if you can't trust ProtonMail from sending
malicious JavaScript code, then you can't use ProtonMail. It might be more
nuanced than that, considering how the client is open and "can" be "built"
by people if wanted.
